const express = require('express');
const app = express();
const port = 3000;

// EJS
app.set('view engine', 'ejs');

// Morgan
const morgan = require('morgan');

// Logger

app.use(morgan('tiny'));

// Express Static
app.use(express.static('public'));

// Router
const indexRoute = require('./routes');

app.use('/', indexRoute);

app.listen(port, () => console.log("server running"));