const express = require('express');
const router = express.Router();
const users = require('../users.json');

// Home
router.get('/', (req, res) => {
    res.render('index', {

    });
})
// End Of Home

// Permainan
router.get('/permainan', (req, res) => {
    res.render('permainan');
})
// End Of Permainan

// Users
router.get('/users', (req, res) => {
    res.json(users);

})
// End Of users

// Login
router.post('/login', (req, res) => {
    res.json(users);

})
// End Of Loggin

router.use('/', (req, res,) => {
    res.status(404);
    res.send('<h1>404<h1>');

})

module.exports = router;